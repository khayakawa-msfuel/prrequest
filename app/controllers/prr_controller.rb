require 'httparty'
require 'json'

class PrrController < ApplicationController
  before_action :read_config, :get_params
  def index
  end

  def format
    @warning = ''
    @us_detail = {}
    @hide_header = true
    if !(@userstory.nil?)
      response = HTTParty.get(@jira_api_uri + '/issue/' + @userstory, :headers => {"Authorization" => @jira_auth})

      if response.code == 200 then
        @us_detail['title'] = response['key'] + ' - ' + response['fields']['summary']
        @us_detail['url'] = @jira_uri + '/' + @userstory
        tasks = response['fields']['subtasks']
        peer_review_task = tasks.find do |task|
          /peer review/i.match(task['fields']['summary'])
        end
        @us_detail['pr_url'] = @jira_uri + '/' + peer_review_task['key'] unless peer_review_task.nil?
      end

      response = HTTParty.get(@gitlab_api_uri + '/merge_requests?source_branch=' + @userstory, :headers => @gitlab_auth)

      @us_detail['merge_requests'] = Array.new
      if response.code == 200 then
        tasks = JSON.parse(response.body)
        tasks.each do |task|
          @us_detail['merge_requests'].push task['web_url'] + '/diffs?w=1'
        end
      end
    end
  end

  def get_params
    @userstory = params[:userstory]
  end
end
